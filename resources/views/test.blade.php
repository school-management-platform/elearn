
                    <div class="col-xl-4 discussion-holder">
                        <div class="card">
                            <!-- <button class="btn btn-primary" id="expandDiscussion">Expand</button> -->
                            <div class="p-4 d-flex justify-content-between align-items-center">
                                <div>
                                    <p class="mb-0 text-navy">Grade 1: Hope</p>
                                    <h1 class="mt-0 text-navy">Discussion Board</h1>
                                </div>
                                
                                <i class="fa fa-expand-alt fa-fw expandDiscussion"></i>
                        </div>


                        <div class="card-body converse-holder">

                            <div class="converse-content mb-5">
                                <div class="chat-user d-flex justify-content-between align-items-center">
                                    <div class="d-flex align-items-center">
                                        <div class="display_picture small" style="background-image: url('https://images.unsplash.com/photo-1549150712-1d243024db80?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1400&q=80');"></div>
                                        <div class="details ml-3">
                                            <p class="m-0 font-size-14 font-weight-medium">Jodie Umali</p>
                                            <p class="m-0 text-muted font-size-14">5 mins. ago</p>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-center align-items-center">
                                        <i class="fa fa-thumbs-up fa-fw font-size-16 text-muted"></i>
                                        <div class="text-primary ml-2">6</div>
                                        <i class="fa fa-ellipsis-v ml-4 text-muted text-opaque"></i>
                                    </div>
                                </div>

                                <div class="converse-content mt-3 font-size-16">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio dolorem quae consectetur tenetur, a tempore laborum voluptatem quas aliquam dolor! Rem quod alias non ullam recusandae laborum quas dignissimos? Perspiciatis?</p>
                                </div>
                            </div>

                            <div class="converse-content mb-5">
                                <div class="chat-user d-flex justify-content-between align-items-center">
                                    <div class="d-flex align-items-center">
                                        <div class="display_picture small" style="background-image: url('https://images.unsplash.com/photo-1549150712-1d243024db80?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1400&q=80');"></div>
                                        <div class="details ml-3">
                                            <p class="m-0 font-size-14 font-weight-medium">Jodie Umali</p>
                                            <p class="m-0 text-muted font-size-14">5 mins. ago</p>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-center align-items-center">
                                        <i class="fa fa-thumbs-up fa-fw font-size-16 text-muted"></i>
                                        <div class="text-primary ml-2">6</div>
                                        <i class="fa fa-ellipsis-v ml-4 text-muted text-opaque"></i>
                                    </div>
                                </div>

                                <div class="converse-content mt-3 font-size-16">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio dolorem quae consectetur tenetur, a tempore laborum voluptatem quas aliquam dolor! Rem quod alias non ullam recusandae laborum quas dignissimos? Perspiciatis?</p>
                                </div>
                            </div>

                            <div class="converse-content mb-5">
                                <div class="chat-user d-flex justify-content-between align-items-center">
                                    <div class="d-flex align-items-center">
                                        <div class="display_picture small" style="background-image: url('https://images.unsplash.com/photo-1549150712-1d243024db80?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1400&q=80');"></div>
                                        <div class="details ml-3">
                                            <p class="m-0 font-size-14 font-weight-medium">Jodie Umali</p>
                                            <p class="m-0 text-muted font-size-14">5 mins. ago</p>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-center align-items-center">
                                        <i class="fa fa-thumbs-up fa-fw font-size-16 text-muted"></i>
                                        <div class="text-primary ml-2">6</div>
                                        <i class="fa fa-ellipsis-v ml-4 text-muted text-opaque"></i>
                                    </div>
                                </div>

                                <div class="converse-content mt-3 font-size-16">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio dolorem quae consectetur tenetur, a tempore laborum voluptatem quas aliquam dolor! Rem quod alias non ullam recusandae laborum quas dignissimos? Perspiciatis?</p>
                                </div>
                            </div>

                            <div class="converse-content mb-5">
                                <div class="chat-user d-flex justify-content-between align-items-center">
                                    <div class="d-flex align-items-center">
                                        <div class="display_picture small" style="background-image: url('https://images.unsplash.com/photo-1549150712-1d243024db80?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1400&q=80');"></div>
                                        <div class="details ml-3">
                                            <p class="m-0 font-size-14 font-weight-medium">Jodie Umali</p>
                                            <p class="m-0 text-muted font-size-14">5 mins. ago</p>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-center align-items-center">
                                        <i class="fa fa-thumbs-up fa-fw font-size-16 text-muted"></i>
                                        <div class="text-primary ml-2">6</div>
                                        <i class="fa fa-ellipsis-v ml-4 text-muted text-opaque"></i>
                                    </div>
                                </div>

                                <div class="converse-content mt-3 font-size-16">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio dolorem quae consectetur tenetur, a tempore laborum voluptatem quas aliquam dolor! Rem quod alias non ullam recusandae laborum quas dignissimos? Perspiciatis?</p>
                                </div>
                            </div>

                            <div class="converse-content mb-5">
                                <div class="chat-user d-flex justify-content-between align-items-center">
                                    <div class="d-flex align-items-center">
                                        <div class="display_picture small" style="background-image: url('https://images.unsplash.com/photo-1549150712-1d243024db80?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1400&q=80');"></div>
                                        <div class="details ml-3">
                                            <p class="m-0 font-size-14 font-weight-medium">Jodie Umali</p>
                                            <p class="m-0 text-muted font-size-14">5 mins. ago</p>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-center align-items-center">
                                        <i class="fa fa-thumbs-up fa-fw font-size-16 text-muted"></i>
                                        <div class="text-primary ml-2">6</div>
                                        <i class="fa fa-ellipsis-v ml-4 text-muted text-opaque"></i>
                                    </div>
                                </div>

                                <div class="converse-content mt-3 font-size-16">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio dolorem quae consectetur tenetur, a tempore laborum voluptatem quas aliquam dolor! Rem quod alias non ullam recusandae laborum quas dignissimos? Perspiciatis?</p>
                                </div>
                            </div>

                        </div>

                        <div class="card-body bg-light-grey d-flex justify-content-center align-items-center">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                </div>