import store from "../store/store";

var _this = this;

const auth = {
    init: self => {
        _this = self;
    },

    is_auth_teacher: (to, from, next) => {
        if (store.state.user != null
        && store.state.user.role == 2) {
            next();
        } else {
                next({
                    name: "login"
                });
        }
    },

    is_guest: (to, from, next) => {
        if (store.state.user == null) next();
        else
            next({
                name: "search"
            });
    }
};

export default auth;
