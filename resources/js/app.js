require('./bootstrap');

import Vue from "vue";

import router from "./router/routes";
import store from "./store/store";

import App from "./pages/layout/App";

const app = new Vue({
    el: "#app",
    store,
    data: {
        ver: "1.00.1",
        base_url: process.env.API_URL
    },

    methods: {
        storage_link(str) {
            return document.querySelector('meta[name="api_url"]').getAttribute('content') + '/storage' + str;
        },

        format_date(date, format) {
            return moment(date).format(format);
        },

        timeago(date) {
            return moment(date).fromNow();
        }
    },
    components: { App },
    router
});