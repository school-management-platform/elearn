import Vue from "vue";
import Vuex from "vuex";
import VuexPersist from "vuex-persist";

import axios from "axios";

Vue.use(Vuex);

const vuexPersist = new VuexPersist({
    key: "lms",
    storage: window.localStorage
});

const url = document.querySelector('meta[name="api_url"]').getAttribute('content')
const base_url = url + "/api";

const store = new Vuex.Store({
    state: {
        user: null,

        // Teacher Data
        teacher: {
            details: null,
            current_class: null
        },

        student: {
            details: null,
            current_class: null,
            current_record: null
        },

        school: null,

        cache: {}
    },
    mutations: {
        setUser(state, payload) {
            state.user = payload;
        },

        setSchool(state, payload) {
            state.school = payload;
        },

        setTeacherDetails(state, payload) {
            state.teacher.details = payload;
        },

        setCurrentClassDetails(state, payload) {
            state.teacher.current_class = payload;
        },

        setStudentDetails(state, payload) {
            state.student.details = payload;
        },

        setStudentCurrentRecord(state, payload) {
            state.student.current_record = payload;
        },

        setStudentCurrentClassDetails(state, payload) {
            state.student.current_class = payload;
        }
    },
    actions: {
        login({}, payload) {
            return axios.post(base_url + "/auth/login", payload);
        },

        getSchool({}, payload) {
            return axios.get(base_url + "/school/get", {
                params: payload,
            });
        },

        // Teacher Actions
        getTeacher({ commit }, payload) {
            return axios.get(base_url + "/teacher/get", {
                params: payload,
                headers: {
                    Authorization: "Bearer " + store.state.user._token.token
                }
            });
        },

        getCurrentClassDetails({ commit }, payload) {
            return axios.get(base_url + "/section/class", {
                params: payload,
                headers: {
                    Authorization: "Bearer " + store.state.user._token.token
                }
            });
        },

        getClassStudents({}, payload) {
            return axios.get(base_url + "/section/class/students", {
                params: payload,
                headers: {
                    Authorization: "Bearer " + store.state.user._token.token
                }
            });
        },

        // Student Actions
        getStudent({ commit }, payload) {
            return axios.get(base_url + "/student/get", {
                params: payload,
                headers: {
                    Authorization: "Bearer " + store.state.user._token.token
                }
            });
        },

        // Virtual Classes
        getVirtualClasses({}, payload) {
            return axios.get(base_url + "/section/class/virtual", {
                params: payload,
                headers: {
                    Authorization: "Bearer " + store.state.user._token.token
                }
            });
        },
        storeVirtualClass({}, payload) {
            return axios.post(
                base_url + "/section/class/virtual/store",
                payload,
                {
                    headers: {
                        Authorization: "Bearer " + store.state.user._token.token
                    }
                }
            );
        },
        updateVirtualClass({}, payload) {
            return axios.post(
                base_url + "/section/class/virtual/update",
                payload,
                {
                    headers: {
                        Authorization: "Bearer " + store.state.user._token.token
                    }
                }
            );
        },
        deleteVirtualClass({}, payload) {
            return axios.post(
                base_url + "/section/class/virtual/delete",
                payload,
                {
                    headers: {
                        Authorization: "Bearer " + store.state.user._token.token
                    }
                }
            );
        },

        // Announcement
        storeAnnouncement({}, payload) {
            return axios.post(
                base_url + "/section/class/announcement/store",
                payload,
                {
                    headers: {
                        Authorization: "Bearer " + store.state.user._token.token
                    }
                }
            );
        },

        // Module
        getModuleClass({}, payload) {
            return axios.get(base_url + "/section/class/module", {
                params: payload,
                headers: {
                    Authorization: "Bearer " + store.state.user._token.token
                }
            });
        },

        storeModuleClass({}, payload) {
            return axios.post(
                base_url + "/section/class/module/store",
                payload,
                {
                    headers: {
                        Authorization: "Bearer " + store.state.user._token.token
                    }
                }
            );
        },

        deleteModuleClass({}, payload) {
            return axios.post(
                base_url + "/section/class/module/delete",
                payload,
                {
                    headers: {
                        Authorization: "Bearer " + store.state.user._token.token
                    }
                }
            );
        },

        // Virtual Classes
        getAssessmentClasses({}, payload) {
            return axios.get(base_url + "/section/class/assessment", {
                params: payload,
                headers: {
                    Authorization: "Bearer " + store.state.user._token.token
                }
            });
        },
        storeAssessmentClass({}, payload) {
            return axios.post(
                base_url + "/section/class/assessment/store",
                payload,
                {
                    headers: {
                        Authorization: "Bearer " + store.state.user._token.token
                    }
                }
            );
        },
        updateAssessmentClass({}, payload) {
            return axios.post(
                base_url + "/section/class/assessment/update",
                payload,
                {
                    headers: {
                        Authorization: "Bearer " + store.state.user._token.token
                    }
                }
            );
        },
        deleteAssessmentClass({}, payload) {
            return axios.post(
                base_url + "/section/class/assessment/delete",
                payload,
                {
                    headers: {
                        Authorization: "Bearer " + store.state.user._token.token
                    }
                }
            );
        },

        // Google Link
        storeGoogleLinkClass({}, payload) {
            return axios.post(
                base_url + "/section/class/google_link/store",
                payload,
                {
                    headers: {
                        Authorization: "Bearer " + store.state.user._token.token
                    }
                }
            );
        },

        // Schedule
        storeScheduleClass({}, payload) {
            return axios.post(
                base_url + "/section/class/schedule/store",
                payload,
                {
                    headers: {
                        Authorization: "Bearer " + store.state.user._token.token
                    }
                }
            );
        },

        // Virtual Classes
        getDiscussionsClass({}, payload) {
            return axios.get(base_url + "/section/class/discussion", {
                params: payload,
                headers: {
                    Authorization: "Bearer " + store.state.user._token.token
                }
            });
        },
        storeDiscussionsClass({}, payload) {
            return axios.post(
                base_url + "/section/class/discussion/store",
                payload,
                {
                    headers: {
                        Authorization: "Bearer " + store.state.user._token.token
                    }
                }
            );
        }
    },

    plugins: [vuexPersist.plugin]
});

export default store;
