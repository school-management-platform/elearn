import Vue from "vue";
import VueRouter from "vue-router";

import auth from "../middlewares/auth";

Vue.use(VueRouter);

import Login from "../pages/public/Login";

// Teacher Items
import TeacherLayout from "../pages/layout/Teacher";
import TeacherIndex from "../pages/teacher/Index";
import TeacherStudent from "../pages/teacher/StudentList";
import TeacherVirtual from "../pages/teacher/VirtualClass";
import TeacherFileLayout from "../pages/teacher/FileManager/Layout";
import TeacherPowerpoint from "../pages/teacher/FileManager/Powerpoint";
import TeacherSupplement from "../pages/teacher/FileManager/Supplements";
import TeacherAssessments from "../pages/teacher/FileManager/Assessments";

// Student Items
import StudentLayout from "../pages/layout/Student";
import StudentIndex from "../pages/student/Index";
import StudentVirtual from "../pages/student/VirtualClass";
import StudentFileLayout from "../pages/student/FileManager/Layout";
import StudentPowerpoint from "../pages/student/FileManager/Powerpoint";
import StudentSupplement from "../pages/student/FileManager/Supplements";
import StudentAssessments from "../pages/student/FileManager/Assessments";

// Error Pages
// import NotFound from "../pages/errors/NotFound";

auth.init(Vue._self);

const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/:school_id",
            name: "login",
            // beforeEnter: auth.is_guest,
            component: Login
        },

        {
            path: "/teacher/:class_id",
            component: TeacherLayout,
            // beforeEnter: auth.is_auth,
            children: [
                {
                    path: "dashboard",
                    name: "dashboard.index",
                    // beforeEnter: auth.is_auth,
                    component: TeacherIndex
                },
                {
                    path: "students",
                    name: "dashboard.students",
                    // beforeEnter: auth.is_auth,
                    component: TeacherStudent
                },
                {
                    path: "virtual",
                    name: "dashboard.virtuals",
                    // beforeEnter: auth.is_auth,
                    component: TeacherVirtual
                },
                {
                    path: "files",
                    component: TeacherFileLayout,
                    children: [
                        {
                            path: "powerpoint",
                            name: "dashboard.files.powerpoint",
                            // beforeEnter: auth.is_auth,
                            component: TeacherPowerpoint
                        },
                        {
                            path: "supplements",
                            name: "dashboard.files.supplement",
                            // beforeEnter: auth.is_auth,
                            component: TeacherSupplement
                        },
                        {
                            path: "assessments",
                            name: "dashboard.files.assessment",
                            // beforeEnter: auth.is_auth,
                            component: TeacherAssessments
                        }
                    ]
                }
            ]
        },

        {
            path: "/student/:class_id",
            component: StudentLayout,
            // beforeEnter: auth.is_auth,
            children: [
                {
                    path: "dashboard",
                    name: "student.index",
                    // beforeEnter: auth.is_auth,
                    component: StudentIndex
                },
                {
                    path: "virtual",
                    name: "student.virtuals",
                    // beforeEnter: auth.is_auth,
                    component: StudentVirtual
                },
                {
                    path: "files",
                    component: StudentFileLayout,
                    children: [
                        {
                            path: "powerpoint",
                            name: "student.files.powerpoint",
                            // beforeEnter: auth.is_auth,
                            component: StudentPowerpoint
                        },
                        {
                            path: "supplements",
                            name: "student.files.supplement",
                            // beforeEnter: auth.is_auth,
                            component: StudentSupplement
                        },
                        {
                            path: "assessments",
                            name: "student.files.assessment",
                            // beforeEnter: auth.is_auth,
                            component: StudentAssessments
                        }
                    ]
                }
            ]
        }

        // {
        //     path: "*",
        //     name: "notfound",
        //     // beforeEnter: auth.is_guest,
        //     component: NotFound
        // }
    ]
});

export default router;
